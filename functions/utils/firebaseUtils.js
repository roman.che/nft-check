const functions = require('firebase-functions');
const region = functions.region('europe-central2');
const https = region.https;


async function handle(handlers, { ...other }) {
    if (!handlers) {
        console.error('handlers is empty');
        return;
    }

    if (!Array.isArray(handlers)) {
        handlers = [handlers];
    }

    let result = {};
    const startTime = performance.now();
    for (const handler of handlers) {
        if (!handler) {
            console.error('handler is empty');
            continue;
        }
        result = (await handler({ ...result, ...other })) || {};
    }

    console.log(`ColdTime: ${performance.now() - startTime}`);
}

exports.onHttp = (handlers) => https.
    onRequest(async (request, response) => handle(handlers, { request, response }));