const { onHttp } = require('../utils/firebaseUtils');
const { PublicKey, Connection, clusterApiUrl } = require('@solana/web3.js');
const { Metaplex } = require('@metaplex-foundation/js');

const connection = new Connection(clusterApiUrl('mainnet-beta'));
const metaplex = Metaplex.make(connection);
const method = 'POST';
const handlers = [
	validateData,
	nftCheck,
	httpResponse
];


exports.nftCheck = onHttp(handlers);

async function validateData({ request }) {
	if (request.method !== method) {
		return { error: 'Not allowed' };
	}

	const { nftAddress } = request.body;
	if (!nftAddress) {
		return { error: 'Missed nft address' };
	}

	return { nftAddress };
}

async function httpResponse({ response, error, result }) {
	if (error) {
		return response.status(400).send({ status: 0, error });
	}

	return response.status(200).send({ status: 1, result });
}

async function nftCheck({ nftAddress, error }) {
	if (error) {
		return { error };
	}

	const startTime = performance.now();

	const mintAddress = new PublicKey(nftAddress);
	const nft = await metaplex.nfts().findByMint({ mintAddress }).run();
	const verifiedCreators = Object.values(nft.creators).every(value => value.verified === true);

	console.log(`HotTime: ${performance.now() - startTime}`);

	return { result: { nftAddress, isValid: verifiedCreators } };
} 