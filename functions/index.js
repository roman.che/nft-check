const { initializeApp } = require('firebase-admin/app');
initializeApp();

exports.http = require('./http');